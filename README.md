<!--
 * @Author: 彭雨
 * @Date: 2020-03-15 10:41:03
 * @LastEditors: 彭雨
 * @LastEditTime: 2020-03-15 11:30:28
 -->
# NodeMCU_v3_ws2812、已弃用，推荐使用：https://github.com/Aircoookie/WLED

#### 介绍
WS2812灯带使用NodeMCU通过mqtt接入home assistant。

#### 准备工作
1. NodeMCU v3
2. Home Assistant服务器
3. mqtt服务器
4. ws2812灯带（Din接NodeMCU的D4引脚）


#### 安装教程

1.  云编译NodeMCU固件，地址：https://nodemcu-build.com/index.php ，除了默认模块以外，需要额外选择：MQTT、WS2812、WS2812 effects这三个模块。
2.  使用：https://github.com/nodemcu/nodemcu-flasher 将固件刷入NodeMCU
3.  使用：http://esp8266.ru/esplorer/ 将init.lua文件上传到NodeMCU（注意修改文件中WiFi信息和mqtt服务器信息，具体看文件注释）

#### 使用说明

1.  在Home Assistant的配置文件中加入：
~~~
light:
  - platform: mqtt
    name: "灯带"
    command_topic: "ws2812/switch"
    state_topic: "ws2812/switchpub"
    rgb_state_topic: "ws2812/setcolorpub"
    rgb_command_topic: "ws2812/setcolor"
    brightness_state_topic: "ws2812/setbrightnesspub"
    brightness_command_topic: "ws2812/setbrightness"
    effect_list: [static, blink, random_color, rainbow, rainbow_cycle, flicker, fire, fire_soft, halloween, circus_combustus, larson_scanner, color_wipe, random_dot, gradient]
    effect_command_topic: "ws2812/seteffect"
    effect_state_topic: "ws2812/seteffectpub"
    optimistic: false
input_number:
  light_speed:
    name: '动画速度'
    min: 1
    max: 255
    step: 1
    icon: mdi:speedometer
~~~

#### 效果截图
![开关和速度调节](https://images.gitee.com/uploads/images/2020/0315/110109_c86f11db_1219033.jpeg "20200315110047.jpg")
![颜色亮度和模式调整](https://images.gitee.com/uploads/images/2020/0315/110211_88191eb4_1219033.jpeg "20200315110158.jpg")